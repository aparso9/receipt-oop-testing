package assignment5;
import Factory.ReceiptFactory;
import interfaces.Receipt;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) throws ParseException{
        //1. Creates a Date object (either from Java API or date entered by user)
        
        //2. Creates a PurchasedItems object(selections made by user)
        
        //3. Constructs a ReceiptFactory object
        
        //4. Prompts user for items to purchase, storing each in PurchasedItems.
        
        //5. Calls the getReceiptMethod of the factory to obtain constructed receipt
        
        //6. Prints receipt by call to prtReceipt method
        
        
        //display all available store items to user
        //TODO - implement
        
        //get all user selections
        //TODO - implement
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        DecimalFormat df = new DecimalFormat("$##.00");
        PurchasedItems items = null;
        Date date = new Date();
        ReceiptFactory receiptFactory = new ReceiptFactory();
        
        Scanner k = new Scanner(System.in);
        int choice = 1;
        while(choice > 0 && choice < 4){
            try{
                System.out.println("Select an option from the menu or "
                        + "enter a number higher or lower to quit: "
                        + "\n1 - Start New Receipt"
                        + "\n2 - Add Items"
                        + "\n3 - Display Receipt");
                choice = k.nextInt();

                switch(choice){
                    case 1: {
                        items = new PurchasedItems();
                        date = new Date();

                        System.out.println("New receipt created");
                        break;
                    }
                    case 2: {
                        if(items != null){
                            boolean yn = true;
                            String option;
                            String itemCode;
                            String description;
                            double price;
                            while(yn){
                                System.out.println("Add item? (y/n): ");
                                k.nextLine();
                                option = k.nextLine();
                                if(!option.equalsIgnoreCase("y")){
                                    yn = false;
                                }
                                else{
                                    System.out.println("Enter item code: ");
                                    itemCode = k.nextLine();
                                    System.out.println("Enter item description: ");
                                    description = k.nextLine();
                                    System.out.println("Enter price of item: ");
                                    try{
                                        price = k.nextDouble();

                                        items.addItem(new StoreItem(itemCode, description, df.format(price)));

                                    }
                                    catch(InputMismatchException e){
                                        System.out.println("Enter a valid option");
                                        k.next();
                                    }
                                }
                            }
                        }
                        else{
                            System.out.println("PurchasedItems is null");
                        }
                        break;
                    }
                    case 3: {
                        if(items!=null){
                            Date testDate = sdf.parse("08/10/2019");
                            receiptFactory.getReceipt(items, testDate).prtReceipt();
                            System.out.println();
                        }
                        else{
                            System.out.println("No receipt found\n");
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
            catch(InputMismatchException e){
                System.out.println("Please enter a valid menu option");
                k.nextLine();
            }
        }
    }
}
