package assignment5;

public class StoreHeader {
    private String str_addr;
    private String zip_code;
    private String state_code;
    private String phone_num;
    private String store_num;
    
    public StoreHeader(String[] config){
        this.str_addr = config[0];
        this.state_code = config[1];
        this.zip_code = config[2];
        this.store_num = config[3];
        this.phone_num = config[4];
    }
    
    public String getStateCode(){
        return state_code;
    }
    
    @Override
    public String toString(){
        return "BEST BUY\t\t\t\t" + "Store #: " + store_num 
                +"\n" + str_addr + ", " + state_code + ", " + zip_code + "\t" + phone_num; 
    }
}
