package assignment5;

import java.util.ArrayList;

public class PurchasedItems {
    private ArrayList<StoreItem> items;
    
    public PurchasedItems(){
        items = new ArrayList();
    }
    
    public void addItem(StoreItem item){
        items.add(item);
    }
    
    public void getItems(){
        items.stream().forEach((item) -> {
            System.out.println(item.toString());
        });
    }
    
    public double getTotalCost(){
        double total = 0;
        String st;
        for (StoreItem item : items) {
            st = item.getItemPrice().replace("$","");
            total += Double.parseDouble(st);
        }
        return total;
    }
    
    public boolean containsItems(String itemCode){
        //TODO - implement this method
        boolean contains = false;
        for(StoreItem item : items){
            if(item.getItemCode().equals(itemCode)){
                contains = true;
            }
        }
        return contains;
    }
}
