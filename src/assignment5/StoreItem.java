package assignment5;

public class StoreItem {
    private String itemCode;
    private String itemDescription;
    private String itemPrice;
    
    public StoreItem(String code, String desc, String price){
        itemCode = code;
        itemDescription = desc;
        itemPrice = price;
    }

    public String getItemCode() {
        return itemCode;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }
    
    @Override
    public String toString(){
        return itemCode +"\t" + itemDescription + "\t\t\t\t" + itemPrice;
    }
}
