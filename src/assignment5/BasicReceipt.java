package assignment5;

import TaxComputation.MDTaxComputation;
import abstract_classes.TaxComputationMethod;
import interfaces.Receipt;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BasicReceipt implements Receipt{
    private StoreHeader store_header;
    private TaxComputationMethod tc;
    private final Date d;
    private final PurchasedItems items;
    private final DecimalFormat df = new DecimalFormat("$#0.00");
    private final DateFormat datef = new SimpleDateFormat("MM/dd/YYYY hh:mm a");
    
    public BasicReceipt(PurchasedItems items, Date d){
        this.items = items;
        this.d = d;
    }
    
    public void setStoreHeader(StoreHeader store_header){
        this.store_header = store_header;
    }
    
    public void setTaxComputationMethod(TaxComputationMethod tc){
        this.tc = tc;
    }
    public String getStoreHeader(){
        return store_header.toString();
    }
    
    @Override
    public void prtReceipt(){
        System.out.println(store_header.toString() 
                + "\n"
                + "\n" + datef.format(d)
                +"\n"
                + "\nItem #");
        items.getItems();
        System.out.println();
        System.out.println("Total Cost: \t\t\t\t" + df.format(items.getTotalCost()) 
                + "\nSales Tax: \t\t\t\t" + df.format(tc.computeTax(items, d)) 
                + "\nTotal: \t\t\t\t\t" + df.format(tc.computeTax(items, d) + items.getTotalCost()));
    }
}
