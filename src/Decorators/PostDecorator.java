package Decorators;

import abstract_classes.Decorator;
import interfaces.AddOn;
import interfaces.Receipt;

public class PostDecorator extends Decorator{
    private AddOn a;
    
    public PostDecorator(Receipt r, AddOn a) {
        super(r, a);
        this.a = a;
    }

    @Override
    public void prtReceipt() {
        callTrailer();
        System.out.println("------------------------------------------------------");
        System.out.println(a.getLines());
    }

}
