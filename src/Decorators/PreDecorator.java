package Decorators;

import abstract_classes.Decorator;
import interfaces.AddOn;
import interfaces.Receipt;

public class PreDecorator extends Decorator {
    private AddOn a;
    
    public PreDecorator(Receipt r, AddOn a) {
        super(r, a);
        this.a = a;
    }
    
    @Override
    public void prtReceipt() {
        System.out.println(a.getLines());
        System.out.println("------------------------------------------------------");
        callTrailer();
    }

}
