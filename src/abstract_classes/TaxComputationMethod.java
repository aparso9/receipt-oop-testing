package abstract_classes;

import assignment5.PurchasedItems;
import java.util.Date;

public abstract class TaxComputationMethod {
    public abstract double computeTax(PurchasedItems items, Date date);
    protected abstract boolean taxHoliday(Date date);
}
