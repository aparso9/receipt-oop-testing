package add_ons;

import assignment5.PurchasedItems;
import interfaces.Rebate;

public class Rebate1406 implements Rebate {

    @Override
    public boolean applies(PurchasedItems items) {
        return items.containsItems("1406");
    }

    @Override
    public String getLines() {
        return "Mail-in Rebate for Item #1406\n" + "Name:\n" + "Address:\n\n" +
                "Mail to: Best Buy Rebates, P.O. Box 1400, Orlando, FL";
    }
    
}
