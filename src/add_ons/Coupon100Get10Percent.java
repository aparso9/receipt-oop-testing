package add_ons;

import assignment5.PurchasedItems;
import interfaces.Coupon;

public class Coupon100Get10Percent implements Coupon{

    @Override
    public boolean applies(PurchasedItems items) {
        return items.getTotalCost() > 100;
    }

    @Override
    public String getLines() {
        return "Best Buy Coupon 10% off next purchase";
    }

}
