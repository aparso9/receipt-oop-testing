package TaxComputation;

import abstract_classes.TaxComputationMethod;
import assignment5.PurchasedItems;
import java.util.Date;

public class COTaxComputation extends TaxComputationMethod{

    @Override
    public double computeTax(PurchasedItems items, Date date) {
        return items.getTotalCost() * 0.029;
    }

    @Override
    protected boolean taxHoliday(Date date) {
        return false;
    }

}
