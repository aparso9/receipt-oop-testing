package TaxComputation;

import abstract_classes.TaxComputationMethod;
import assignment5.PurchasedItems;
import java.util.Date;

public class NoSalesTaxComputation extends TaxComputationMethod{

    @Override
    public double computeTax(PurchasedItems items, Date date) {
        return 0;
    }

    @Override
    protected boolean taxHoliday(Date date) {
        return false;
    }
    
}
