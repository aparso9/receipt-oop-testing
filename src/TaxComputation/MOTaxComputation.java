package TaxComputation;

import abstract_classes.TaxComputationMethod;
import assignment5.PurchasedItems;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MOTaxComputation extends TaxComputationMethod{
    private DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    @Override
    public double computeTax(PurchasedItems items, Date date) {
        double salesTax = 0;
        if(!taxHoliday(date)){
            salesTax = 0.04225 * items.getTotalCost();
        }
        return salesTax;
    }
    @Override
    public boolean taxHoliday(Date d) {
        boolean isTaxHoliday = false;
        try{
            df.parse(df.format(d));
            Date startTaxHoliday1 = df.parse("04/19/2019");
            Date endTaxHoliday1 = df.parse("04/25/2019");
            Date startTaxHoliday2 = df.parse("08/02/2019");
            Date endTaxHoliday2 = df.parse("08/04/2019");
            
            if(d.after(startTaxHoliday1) && d.before(endTaxHoliday1)|| 
                    d.after(startTaxHoliday2) && d.before(endTaxHoliday2) || 
                    d.equals(startTaxHoliday1) || d.equals(startTaxHoliday2) ||
                    d.equals(endTaxHoliday1) || d.equals(endTaxHoliday2)){
                isTaxHoliday = true;
            }       
        }
        catch(ParseException e){
            System.out.println("Error, date not found");
        }
        return isTaxHoliday;
    }
}
