package TaxComputation;

import abstract_classes.TaxComputationMethod;
import assignment5.PurchasedItems;
import java.util.Date;

public class AZTaxComputation extends TaxComputationMethod{

    @Override
    public double computeTax(PurchasedItems items, Date date) {
        double salesTax = 0;
        if(!taxHoliday(date)){
            salesTax = 0.056 * items.getTotalCost();
        }
        return salesTax;
    }

    @Override
    protected boolean taxHoliday(Date date) {
        return false;
    }

}
