package TaxComputation;

import abstract_classes.TaxComputationMethod;
import assignment5.PurchasedItems;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CTTaxComputation extends TaxComputationMethod{
    private DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    @Override
    public double computeTax(PurchasedItems items, Date date) {
        double salesTax = 0;
        if(!taxHoliday(date)){
            salesTax = 0.0635 * items.getTotalCost();
        }
        return salesTax;
    }
    @Override
    public boolean taxHoliday(Date d) {
        boolean isTaxHoliday = false;
        try{
            df.parse(df.format(d));
            Date startTaxHoliday1 = df.parse("08/18/2019");
            Date endTaxHoliday1 = df.parse("08/24/2019");
            
            if(d.after(startTaxHoliday1) && d.before(endTaxHoliday1)||  
                    d.equals(startTaxHoliday1) ||
                    d.equals(endTaxHoliday1)){
                isTaxHoliday = true;
            }       
        }
        catch(ParseException e){
            System.out.println("Error, date not found");
        }
        return isTaxHoliday;
    }
}
