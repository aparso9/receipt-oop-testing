package TaxComputation;

import abstract_classes.TaxComputationMethod;
import assignment5.PurchasedItems;
import java.util.Date;

public class NJTaxComputation extends TaxComputationMethod{

    @Override
    public double computeTax(PurchasedItems items, Date date) {
        return items.getTotalCost() * 0.06625;
    }

    @Override
    protected boolean taxHoliday(Date date) {
        return false;
    }
    
}