package TaxComputation;

import abstract_classes.TaxComputationMethod;
import assignment5.PurchasedItems;
import java.util.Date;

public class CATaxComputation extends TaxComputationMethod{
    @Override
    public double computeTax(PurchasedItems items, Date date) {
        double salesTax = 0;
        if(!taxHoliday(date)){
            salesTax = 0.0725 * items.getTotalCost();
        }
        return salesTax;
    }
    
    @Override
    public boolean taxHoliday(Date d) {
        boolean isTaxHoliday = false;
        return isTaxHoliday;
    }
}
