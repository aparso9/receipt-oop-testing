package TaxComputation;

import abstract_classes.TaxComputationMethod;
import assignment5.PurchasedItems;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MSTaxComputation extends TaxComputationMethod{
    private DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    @Override
    public double computeTax(PurchasedItems items, Date date) {
        double salesTax = 0;
        if(!taxHoliday(date)){
            salesTax = 0.07 * items.getTotalCost();
        }
        return salesTax;
    }
    @Override
    public boolean taxHoliday(Date d) {
        boolean isTaxHoliday = false;
        try{
            df.parse(df.format(d));
            Date startTaxHoliday1 = df.parse("07/26/2019");
            Date endTaxHoliday1 = df.parse("07/27/2019");
            Date startTaxHoliday2 = df.parse("08/30/2019");
            Date endTaxHoliday2 = df.parse("09/01/2019");
            
            if(d.equals(startTaxHoliday1) || d.equals(startTaxHoliday2) ||
                    d.equals(endTaxHoliday1) || d.equals(endTaxHoliday2)){
                isTaxHoliday = true;
            }       
        }
        catch(ParseException e){
            System.out.println("Error, date not found");
        }
        return isTaxHoliday;
    }
}
