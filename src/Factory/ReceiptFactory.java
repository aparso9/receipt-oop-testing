package Factory;

import TaxComputation.*;
import abstract_classes.*;
import add_ons.*;
import assignment5.*;
import Decorators.*;
import interfaces.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.Scanner;
import java.util.ArrayList;

public class ReceiptFactory {
    StoreHeader store_header;
    private TaxComputationMethod[] taxComputationObjs;
    private AddOn[] addOns;
    private String[] f = new String[5];
    
    public ReceiptFactory(){
        //1. Populate array of TaxComputationMethod objects and array of AddOn obects(as if downloaded from Best Buy Website)
        TaxComputationMethod CA = new CATaxComputation();
        TaxComputationMethod MA = new MATaxComputation();
        TaxComputationMethod MD = new MDTaxComputation();
        TaxComputationMethod no = new NoSalesTaxComputation();
        TaxComputationMethod AL = new ALTaxComputation();
        TaxComputationMethod AR = new ARTaxComputation();
        TaxComputationMethod CT = new CTTaxComputation();
        TaxComputationMethod IA = new IATaxComputation();
        TaxComputationMethod MS = new MSTaxComputation();
        TaxComputationMethod MO = new MOTaxComputation();
        TaxComputationMethod NM = new NMTaxComputation();
        TaxComputationMethod OH = new OHTaxComputation();
        TaxComputationMethod OK = new OKTaxComputation();
        TaxComputationMethod SC = new SCTaxComputation();
        TaxComputationMethod TN = new TNTaxComputation();
        TaxComputationMethod TX = new TXTaxComputation();
        TaxComputationMethod VA = new VATaxComputation();
        TaxComputationMethod AZ = new AZTaxComputation();
        TaxComputationMethod CO = new COTaxComputation();
        TaxComputationMethod six = new SixPercentTaxComputation();
        TaxComputationMethod four = new FourPercentTaxComputation();
        TaxComputationMethod IL = new ILTaxComputation();
        TaxComputationMethod sixFive = new SixFivePercentTaxComputation();
        TaxComputationMethod five = new FivePercentTaxComputation();
        TaxComputationMethod seven = new SevenPercentTaxComputation();
        TaxComputationMethod LA = new LATaxComputation();
        TaxComputationMethod ME = new METaxComputation();
        TaxComputationMethod MN = new MNTaxComputation();
        TaxComputationMethod NE = new NETaxComputation();
        TaxComputationMethod NJ = new NJTaxComputation();
        TaxComputationMethod NC = new NCTaxComputation();
        TaxComputationMethod SD = new SDTaxComputation();
        TaxComputationMethod UT = new UTTaxComputation();
        
        taxComputationObjs = new TaxComputationMethod[]{CA, MA, MD, no, AL, AR, 
            CT, IA, MS, MO, NM, OH, OK, SC, TN, TX, VA, AZ, CO, six, four, IL, 
            sixFive, five, seven, ME, MN, NE, NJ, NC, SD, UT};
        
        AddOn coupon100Get10Percent = new Coupon100Get10Percent();
        AddOn holidayGreeting = new HolidayGreeting();
        AddOn rebate1406 = new Rebate1406();
        addOns = new AddOn[]{coupon100Get10Percent, holidayGreeting, rebate1406};
        //2. Reads Config file to create and save StoreHeader object to be used on all receipts
        int i = 0;
        try{
            File file= new File("config.txt");
            Scanner fileScanner = new Scanner(file);
            while(fileScanner.hasNext()){
                f[i] = fileScanner.nextLine();
                i++;
            }
        }
        catch(FileNotFoundException e){
            System.out.println("Config file not found");
        }
        //3. Based on the State code (e.g. MD) creates and stores appropriate StateComputation object to be used on all receipts
    }
    
    public Receipt getReceipt(PurchasedItems items, Date d){
        Receipt receipt = new BasicReceipt(items, d);
        //1. Sets the current date of the receipt
        //2. Sets StoreHeader object of the BasicReceipt(by call to SetStoreHeader method of BasicReceipt class)
        StoreHeader sh = new StoreHeader(f);
        ((BasicReceipt)receipt).setStoreHeader(sh);
        //3. Sets the TaxComputationMethod object of the BasicReceipt
        switch(sh.getStateCode()){
            case "CA":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[0]);
                break;
            }
            case "MA":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[1]);
                break;
            }
            case "MD":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[2]);
                break;
            }
            case "AK":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[3]);
            }
            case "DE":{
                
            }
            case "MT":{
                
            }
            case "NH":{
                
            }
            case "OR":{
                break;
            }
            case "AL":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[4]);
                break;
            }
            case "AR":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[5]);
                break;
            }
            case "CT":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[6]);
                break;
            }
            case "IA":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[7]);
                break;
            }
            case "MS":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[8]);
                break;
            }
            case "MO":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[9]);
                break;
            }
            case "NM":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[10]);
                break;
            }
            case "OH":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[11]);
                break;
            }
            case "OK":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[12]);
                break;
            }
            case "SC":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[13]);
                break;
            }
            case "TN":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[14]);
                break;
            }
            case "TX":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[15]);
                break;
            }
            case "VA":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[16]);
                break;
            }
            case "AZ":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[17]);
                break;
            }
            case "CO":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[18]);
                break;
            }
            case "DC":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[19]);
            }
            case "FL":{
                
            }
            case "ID":{
                
            }
            case "KY":{
                
            }
            case "MI":{
                
            }
            case "PA":{
                
            }
            case "VT":{
                
            }
            case "WV":{
                break;
            }
            case "GA":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[20]);
            }
            case "HI":{
                
            }
            case "NY":{
                
            }
            case "WY":{
                break;
            }
            case "IL":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[21]);
                break;
            }
            case "KS":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[22]);
            }
            case "WA":{
                break;
            }
            case "WI":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[23]);
            }
            case "ND":{
                break;
            }
            case "IN":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[24]);
            }
            case "RI":{
                break;
            }
            case "LA":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[25]);
                break;
            }
            case "ME":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[26]);
                break;
            }
            case "MN":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[27]);
                break;
            }
            case "NE":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[28]);
                break;
            }
            case "NJ":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[29]);
                break;
            }
            case "NC":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[30]);
                break;
            }
            case "SD":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[31]);
                break;
            }
            case "UT":{
                ((BasicReceipt)receipt).setTaxComputationMethod(taxComputationObjs[32]);
                break;
            }
            default:{
                break;
            }
        }
        /*4. Traverses over all AddOn objects, calling the applies method of each. If an AddOn object applies, 
        then determine if the AddOn is of type SecondaryHeader, Rebate, or Coupon. If of type SecondaryHeader,
        then create a PreDecorator for the AddOn. If of type Rebate, or Coupon, then creates a PostDecorator*/
        for (AddOn addOn : addOns) {
            if (addOn.applies(items)) {
                if(addOn instanceof SecondaryHeading){
                    receipt = new PreDecorator(receipt, addOn);
                }
                else{
                    receipt = new PostDecorator(receipt, addOn);
                }
            }
        }
        //5. Links in the decorator object based on the Decorator design pattern
        
        //6. Returns decorated Basic Receipt object as type Receipt
        
        return receipt;
    }
}
